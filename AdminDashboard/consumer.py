import pika, json, os, django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "AdminDashboard.settings")
django.setup()

from products.models import Product



params = pika.URLParameters('amqps://hsvpmwol:sfpB63XcJ3XmioUzmjN9i3cMOkslp_Ux@hawk.rmq.cloudamqp.com/hsvpmwol')

connection = pika.BlockingConnection(params)

channel = connection.channel()

channel.queue_declare(queue='admin')


def callback(ch, method, properties, body):
    print('Received in admin')
    id = json.loads(body)
    print(id)
    product = Product.objects.get(id=id)
    product.likes += 1
    product.save()
    print("Product likes increase")


channel.basic_consume(queue='admin', on_message_callback=callback, auto_ack=True)

print('Started Consuming')

channel.start_consuming()

channel.close()
