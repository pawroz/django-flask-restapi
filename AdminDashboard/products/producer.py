import pika, json

params = pika.URLParameters('amqps://hsvpmwol:sfpB63XcJ3XmioUzmjN9i3cMOkslp_Ux@hawk.rmq.cloudamqp.com/hsvpmwol')

connection = pika.BlockingConnection(params)

channel = connection.channel()


def publish(method, body):
    properties = pika.BasicProperties(method)
    channel.basic_publish(exchange='', routing_key='main', body=json.dumps(body), properties=properties)
